# README #

This is the parent POM for Maven projects

* current version 1.0.0

### How do I get set up? ###

* git clone of the project
* go to the project directory and run the following Maven command
```
#!maven

mvn clean install
```